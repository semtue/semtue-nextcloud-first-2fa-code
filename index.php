<?php

require("./conf/config.php");

if ( isset($_POST['username']) && isset($_POST['password']) ) {
	$username = $_POST['username'];
	$password = $_POST['password'];

	$ds=ldap_connect($ldapconfig['host'], $ldapconfig['port']);

	ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);
	ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);
	ldap_set_option($ds, LDAP_OPT_NETWORK_TIMEOUT, 10);

	$dn="uid=".$username.",".$ldapconfig['usersdn'].",".$ldapconfig['basedn'];

	if ($bind=ldap_bind($ds, $dn, $password)) {
		$filter = "(uid=".$username.")";
		$result = ldap_search($ds, $dn, $filter) or exit("Unable to search LDAP server");
		$entries = ldap_get_entries($ds, $result);
		ldap_unbind($ldap);
		$LDmail = $entries[0]["mail"][0];
		$LDuser = $entries[0]["uid"][0];

		$LDmaildomain = substr(strrchr($LDmail, "@"), 1);
		if ( ! in_array($LDmaildomain, $config['maildomains'] )) {
			exit ("Maildomain error.");
		}
		$outstring = "$LDuser;$LDmail";
		$outfilename = substr(hash('md5', $LDuser), 0, 10);
		$donefile = $config['donedir'] ."/" . $outfilename;
		if ( file_exists($donefile) ) {
			$message  = "<p>Der Code für dieses Konto wurde bereits angefordert.</p>";
			$mclass   = "class='error'";
		} else {
			$outfile = $config['workdir'] ."/" . $outfilename;
			file_put_contents($outfile,$outstring);
			$message  = "<p>Ihr Code wurde zum Versand vorbereitet, schauen Sie ihn Ihre Mailbox.</p>";
			$mclass   = "class='ok'";
		}
	} else {
		
		$message  = "<p>Anmeldung fehlgeschlagen!</p>";
		$mclass   = "class='error'";
		
	}
} else {
	$message  = "<p>Hier können Sie einmalig den ersten Einmalcode zur Anmeldung an der Seminarcloud anfordern.</p>";
	$message .= "<p>Bitte melden Sie sich mit Ihren Zugangsdaten aus dem Seminarnetz an. Der Code wird Ihnen an Ihre Seminarmailadresse geschickt</p>";
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>2FA Init: Erster Code</title>
  <link href="css/style.css" rel="stylesheet" type="text/css"></link>
</head>
<body>
<div id="ausgabe">
<div id="logo">
	<img src="https://wolke.semtue.de/index.php/apps/theming/image/logo?useSvg=1&v=5" width="200" />
</div>
<div id="message" <?php echo $mclass; ?>>
<?php 
	echo $message;
?>
</div>
<form action="" method="post">
<input type="text" name="username" id="user" placeholder="Benutzername" aria-label="Benutzername" value="" autofocus="" autocomplete="on" autocapitalize="none" autocorrect="off" required="">
<input type="password" name="password" id="password" value="" placeholder="Passwort" aria-label="Passwort" autocomplete="on" autocapitalize="none" autocorrect="off" required="">
<input type="submit" value="Code anfordern">
</form>
</div>
</body>
</html>
